# Preprocessing Density Of States (DOS)

This script prepares the files to graph the DOS. It's Subtract the Fermi energy value from the energy column, change the signs to pdosdw, and to normalize pdosup and pdosdw.<br>

This code it's written in python 3.

>>>
##### Note 1: 
This script makes use of the numpy module.
>>>

## Way to execute the code
The file and the preprocessing_DOS.py script must be in the same directory.<br><br>

The script can be run with: **python3 preprocessing_DOS.py**.<br>
Also, you can also use **chmod u + xr preprocessing_DOS.py** and run with **./preprocessing_DOS.py**. With this option, the script runs with python 3.<br><br>

