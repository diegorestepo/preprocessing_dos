#!/bin/python3
# -*- coding: utf-8 -*-

'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''

import numpy as np


def fermi():
    valor_fermi = float(input('Enter the Fermi energy value: '))

    return valor_fermi


def modificar_pdos(archivo, valor_fermi):
    col_up, col_dw = _valor_columna(archivo)
    nueva_energia = _modificar_energia(archivo, valor_fermi)
    nueva_pdosup = _modificar_pdosup(archivo, col_up)
    nueva_pdosdw = _modificar_pdosdw(archivo, col_dw)

    return nueva_energia, nueva_pdosup, nueva_pdosdw


def _modificar_energia(archivo, valor_fermi):
    colarray = _leer_columna(archivo, 0)
    nueva_energia = np.subtract(colarray, valor_fermi)

    return nueva_energia


def _modificar_pdosup(archivo, col_up):
    nueva_pdosup = _leer_columna(archivo, col_up)

    return nueva_pdosup


def _modificar_pdosdw(archivo, col_dw):
    colarray = _leer_columna(archivo, col_dw)
    nueva_pdosdw = np.dot(colarray, -1.0)

    return nueva_pdosdw


def _leer_columna(archivo, columna):
    archivo.seek(0)
    filas = archivo.readlines()
    filas = filas[1:len(filas)]

    col = []
    for x in filas:
        col.append(x.split()[columna])

    colarray = np.asarray(col).astype(float)

    return colarray


def _valor_columna(archivo):
    archivo.seek(0)
    filas = archivo.readline()
    primera_fila = filas.split()
    columnas = primera_fila[3:len(primera_fila)]

    cont = 0
    for x in columnas:
        cont += 1
        if x == 'pdosup(E)':
            col_up = cont
        elif x == 'pdosdw(E)':
            col_dw = cont

    return col_up, col_dw


def escribir_output_file(archivo_salida, nueva_energia, nueva_pdosup, nueva_pdosdw):
    nueva_energia_list = nueva_energia.tolist()
    nueva_pdosup_list = nueva_pdosup.tolist()
    nueva_pdosdw_list = nueva_pdosdw.tolist()

    norm_pdosup_list, norm_pdosdw_list = _normalizar(
        nueva_pdosup, nueva_pdosdw)

    with open(archivo_salida, 'w+') as f:
        f.write(
            '# E (eV)          pdosup(E)       pdosdw(E)    norm_pdosup(E)  norm_pdosdw(E)\n')
        for i in range(0, int(len(nueva_energia))):
            f.write('{energia:>10.6f}      {pdosup:>10.6f}      {pdosdw:>10.6f}      {norm_pdosup:>10.6f}      {norm_pdosdw:>10.6f}\t\n'.format(
                energia=nueva_energia_list[i], pdosup=nueva_pdosup_list[i], pdosdw=nueva_pdosdw_list[i], norm_pdosup=norm_pdosup_list[i], norm_pdosdw=norm_pdosdw_list[i]))


def _normalizar(nueva_pdosup, nueva_pdosdw):
    num_atomos = int(input('Enter the number of atoms: '))

    area_up = _area(nueva_pdosup)
    area_dw = _area(nueva_pdosdw)

    norm_pdosup = np.dot(nueva_pdosup, num_atomos)
    norm_pdosup = np.divide(norm_pdosup, area_up)

    norm_pdosdw = np.dot(nueva_pdosdw, num_atomos)
    norm_pdosdw = np.divide(norm_pdosdw, area_dw)

    norm_pdosup_list = norm_pdosup.tolist()
    norm_pdosdw_list = norm_pdosdw.tolist()

    return norm_pdosup_list, norm_pdosdw_list


def _area(nueva_pdos):
    area_pdos = np.trapz(nueva_pdos)
    area_pdos = np.divide(area_pdos, 10)
    area_pdos = np.absolute(area_pdos)

    return area_pdos


def main():
    fichero = input('Enter the file name: ')
    valor_fermi = fermi()
    archivo_salida = input('Enter the name of the output file: ')

    with open(fichero) as archivo:
        fila = archivo.read()
        nueva_energia, nueva_pdosup, nueva_pdosdw = modificar_pdos(
            archivo, valor_fermi)

    escribir_output_file(archivo_salida, nueva_energia,
                         nueva_pdosup, nueva_pdosdw)

if __name__ == '__main__':
    main()
